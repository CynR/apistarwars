function searchByName() {
    //var input_var = document.getElementById('name');
    let input_var = document.getElementById('numPerson');
    let personNumber = input_var.nodeValue;
    fetch(`https://swapi.co/api/people/${personNumber}`).then(function (response) {
        return response.json();
    }).then(function (json) {
        document.getElementById('answerSearched').innerHTML =
            `<ul style="list-style: none;"> 
                    <li> Charcter's  ${personNumber}  Name: ${json.name} </li>
                    <li> Films where he plays:  </li>
                 </ul>
                <div id="films"></div>`;
        parcourirFilms(json.films)
    });
}

function parcourirFilms(films) {
    for (let numberFilm = 0; numberFilm < films.length; numberFilm++) {
        fetch(films[numberFilm]).then(function (response) {
            return response.json();
        }).then(function (json) {
                var filmList = document.createElement("divFilms");
                filmList.innerHTML = `<ul> <li> ${json.title} </li> </ul>`;
                document.getElementById('films').appendChild(filmList);
            }
        );
    }
}


function searchByShips() {
    let input_var = document.getElementById('numStarship');
    let starshipNumber = input_var.nodeValue;
    fetch(`https://swapi.co/api/starships/${starshipNumber}`).then(function (response) {
        return response.json();
    }).then(function (json) {
        document.getElementById('answerSearched').innerHTML =
            `<ul>
                <li>Starship's ${starshipNumber} Name: ${json.name} </li>
                <li>Starship's ${starshipNumber} Model: ${json.model} </li>
                <li>Starship's ${starshipNumber} Manufacturer: ${json.manufacturer} </li>
            </ul>`
    });
}

function searchByPlanets() {
    let input_var = document.getElementById('numPlanets');
    let planetNumber = input_var.nodeValue;
    fetch(`https://swapi.co/api/starships/${planetNumber}`).then(function (response) {
        return response.json();
    }).then(function (json) {
        document.getElementById('answerSearched').innerHTML =
            `<ul>
                <li>Starship's ${planetNumber} Name: ${json.climate} </li>
                <li>Starship's ${planetNumber} Model: ${json.name} </li>
                <li>Starship's ${planetNumber} Manufacturer: ${json.terrain} </li>
            </ul>`
    });
}