function searchByName() {
    //var input_var = document.getElementById('name');
    var input_var = document.getElementById('numPerson');
    var personNumber = input_var.nodeValue;
    fetch("https://swapi.co/api/people/" + personNumber).then(function (response) {
        return response.json();
    }).then(function (json) {
        document.getElementById('answerSearched').innerHTML =
            "<ul style=\"list-style: none;\"> \n                    <li> Charcter's  " + personNumber + "  Name: " + json.name + " </li>\n                    <li> Films where he plays:  </li>\n                 </ul>\n                <div id=\"films\"></div>";
        parcourirFilms(json.films);
    });
}
function parcourirFilms(films) {
    for (var numberFilm = 0; numberFilm < films.length; numberFilm++) {
        fetch(films[numberFilm]).then(function (response) {
            return response.json();
        }).then(function (json) {
            var filmList = document.createElement("divFilms");
            filmList.innerHTML = "<ul> <li> " + json.title + " </li> </ul>";
            document.getElementById('films').appendChild(filmList);
        });
    }
}
function searchByShips() {
    var input_var = document.getElementById('numStarship');
    var starshipNumber = input_var.nodeValue;
    fetch("https://swapi.co/api/starships/" + starshipNumber).then(function (response) {
        return response.json();
    }).then(function (json) {
        document.getElementById('answerSearched').innerHTML =
            "<ul>\n                <li>Starship's " + starshipNumber + " Name: " + json.name + " </li>\n                <li>Starship's " + starshipNumber + " Model: " + json.model + " </li>\n                <li>Starship's " + starshipNumber + " Manufacturer: " + json.manufacturer + " </li>\n            </ul>";
    });
}
function searchByPlanets() {
    var input_var = document.getElementById('numPlanets');
    var planetNumber = input_var.nodeValue;
    fetch("https://swapi.co/api/starships/" + planetNumber).then(function (response) {
        return response.json();
    }).then(function (json) {
        document.getElementById('answerSearched').innerHTML =
            "<ul>\n                <li>Starship's " + planetNumber + " Name: " + json.climate + " </li>\n                <li>Starship's " + planetNumber + " Model: " + json.name + " </li>\n                <li>Starship's " + planetNumber + " Manufacturer: " + json.terrain + " </li>\n            </ul>";
    });
}
